<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('user_agent');
	}

    public function ci_sessions_gc()
    {
        $this->load->database();

        $sess_expiration = config_item('sess_expiration');
        $sess_save_path  = config_item('sess_save_path');

        if( $sess_expiration > 0 )
        {
            $epoch_expired = time() - $sess_expiration;
            $this->db->where('timestamp <', $epoch_expired)
                ->delete( $sess_save_path );
        }
    }

}

/* End of file Welcome.php */
/* Location: ./application/controllers/Welcome.php */