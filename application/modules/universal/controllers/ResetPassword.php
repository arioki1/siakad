<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResetPassword extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation', 'template', 'user_agent'));
        $this->load->model('Maccount', 'account');
    }

    public function index()
    {
        $data = array(
            'title' => 'Sistem Informasi Akademik - STIK Siti khadijah',
        );

        $this->form_validation->set_rules('username', 'Isi Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password Baru', 'trim|min_length[8]|max_length[12]');
        $this->form_validation->set_rules('password-ulangi', 'Ini', 'trim|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('reset-password', $data);
        } else {

            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $type = $this->_get_type_account($username);

            if ($type == "admin") {
                $account = $this->_get_account_admin($username);
                if ($account == TRUE) {
                    $data = array(
                        'password' => password_hash($password, PASSWORD_DEFAULT),
                        'forgot_key' => 0
                    );
                    $where = array('username' => $username);
                    $this->account->update_password($where,$data,'users');
                    redirect(base_url());
                }else{
                    // set error alert
                    $this->template->alert(
                        'Gagal Reset Password Silahkan Hub Admin',
                        array('type' => 'danger', 'icon' => 'times')
                    );
                }
            } else if ($type == "dosen") {
                $account = $this->_get_account_dosen($username);
                if ($account[0] == TRUE) {
                    $data = array(
                        'password' => password_hash($password, PASSWORD_DEFAULT),
                        'forgot_key' => 0
                    );
                    $where = array('lecturer_id' => $account[1]->lecturer_id);
                    $this->account->update_password($where,$data,'lecturer_accounts');
                    redirect(base_url());
                }else{
                    // set error alert
                    $this->template->alert(
                        'Gagal Reset Password',
                        array('type' => 'danger', 'icon' => 'times')
                    );
                }

            } else if ($type == "mahasiswa") {
                $account = $this->_get_account_mahasiswa($username);
                if ($account[0] == TRUE) {
                    $data = array(
                        'password' => password_hash($password, PASSWORD_DEFAULT),
                        'forgot_key' => 0
                    );
                    $where = array('account_student_id' => $account[1]->student_id);
                    $this->account->update_password($where,$data,'students_accounts');
                    redirect(base_url());
                }else{
                    // set error alert
                    $this->template->alert(
                        'Gagal Reset Password',
                        array('type' => 'danger', 'icon' => 'times')
                    );
                }
            } else {
                // set error alert
                $this->template->alert(
                    'Username tidak valid.',
                    array('type' => 'danger', 'icon' => 'times')
                );
                $this->load->view('reset-password', $data);
            }

            $this->load->view('reset-password', $data);
        }
    }

    /**
     * Take a data type account
     *
     * @param String (username)
     * @access private
     * @return Object
     **/
    private
    function _get_type_account($param = 0)
    {
        $query_admin = $this->db->query("SELECT username FROM users WHERE username = ?", array($param));
        $query_siswa = $this->db->query("SELECT npm FROM students WHERE npm = ?", array($param));
        $query_dosen = $this->db->query("SELECT lecturer_code FROM lecturer WHERE lecturer_code = ?", array($param));

        if ($query_admin->num_rows() == 1) {
            return "admin";
        } else if ($query_siswa->num_rows() == 1) {
            return "mahasiswa";
        } else if ($query_dosen->num_rows() == 1) {
            return "dosen";
        } else {
            return false;
        }
    }

    /**
     * Take a data admin
     *
     * @param String (username)
     * @access private
     * @return Object
     **/
    private
    function _get_account_admin($param = 0)
    {
        // get query prepare statmennts
        $query = $this->db->query("SELECT users.username, users.forgot_key 
                                        FROM users  WHERE forgot_key = '1' 
                                        AND username = ?", array($param));

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Take a data Dosen account
     *
     * @param String (username)
     * @access private
     * @return Object
     **/
    private
    function _get_account_dosen($param = 0)
    {
        // get query prepare statmennts
        $query = $this->db->query("SELECT lecturer.lecturer_id, lecturer_accounts.forgot_key, lecturer.lecturer_code
                                        FROM lecturer INNER JOIN lecturer_accounts 
                                        ON lecturer.lecturer_id = lecturer_accounts.lecturer_account_id 
                                        WHERE lecturer_accounts.forgot_key = '1' 
                                        AND lecturer.lecturer_code = ?", array($param));

        if ($query->num_rows() == 1) {
            return array(true,$query->row());
        } else {
            return array(false,'');
        }
    }

    /**
     * Take a data Mahasiswa account
     *
     * @param Integer (NPM)
     * @access private
     * @return Object
     **/
    private
    function _get_account_mahasiswa($param = 0)
    {
        // get query prepare statmennts
        $query = $this->db->query("SELECT students.student_id, students.npm, students_accounts.forgot_key
                                        FROM students 
                                        INNER JOIN students_accounts 
                                        ON students.student_id = students_accounts.account_student_id 
                                        WHERE students_accounts.forgot_key = '1' 
                                        AND students.npm = ?", array($param));
        if ($query->num_rows() == 1) {
            return array(true,$query->row());
        } else {
            return array(false,'');
        }
    }


}

/* End of file Login.php */
/* Location: ./application/modules/Admin/controllers/Login.php */