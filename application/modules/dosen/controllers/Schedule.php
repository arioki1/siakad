<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Jadwal Kuliah Crud Controller
 *
 * @package Bag. Dosen
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Schedule extends Dosen
{
	public $data;

	public $semester;

	public $thn_akademik;

	public function __construct()
	{
		parent::__construct();
		
		$this->breadcrumbs->unshift(1, 'Jadwal Kuliah', "akademik/schedule");

		$this->load->model('mschedule', 'schedule');

		$this->load->model(array());

		$this->load->js(base_url("assets/app/akademik/schedule.js"));

		$this->semester = $this->input->get('semester');

		$this->thn_akademik = $this->input->get('thn_akademik');
	}

	public function index()
	{
		$this->page_title->push('Jadwal Kuliah', 'Lihat Jadwal Perkuliahan');

		$this->breadcrumbs->unshift(2, 'Lihat Jadwal Kuliah', "akademik/schedule");

		$this->form_validation->set_data($this->input->get());

		$this->form_validation->set_rules('thn_akademik', 'Tahun Akademik', 'trim|required');
		$this->form_validation->set_rules('semester', 'Semester', 'trim|required');

		$this->form_validation->run();

		// set pagination
		$config = $this->template->pagination_list();

		$config['base_url'] = site_url(
			"dosen/schedule?per_page="
		);

		$config['per_page'] = (!$this->input->get('per_page')) ? 20 : $this->input->get('per_page');
		$config['total_rows'] = $this->schedule->get_all(null, null, 'num');

		$this->pagination->initialize($config);

		$this->data = array(
			'title' => "Lihat Jadwal Perkuliahan", 
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
			'jadwal_kuliah' => $this->schedule->get_all($config['per_page'], $this->input->get('page')),
			'jumlah_jadwal' => $config['total_rows']
		);

		$this->template->view('schedule/data-jadwal-kuliah', $this->data);
	}


	public function get_print()
	{
		$per_page = (!$this->input->get('per_page')) ? 20 : $this->input->get('per_page');

		$this->data = array(
			'title' => "Jadwal Kuliah Semester ".ucfirst($this->schedule->semester)." Tahun Akademik ".$this->schedule->thn_akademik, 
			'jadwal_kuliah' => $this->schedule->get_all($per_page, $this->input->get('page')),
			'jumlah_jadwal' => $this->schedule->get_all(null, null, 'num')
		);

		$this->load->view('schedule/print-jadwal', $this->data);
	}

}

/* End of file Schedule.php */
/* Location: ./application/modules/akademik/controllers/Schedule.php */