<div class="row">
    <div class="col-md-8 col-md-offset-2 col-xs-12"><?php echo $this->session->flashdata('alert'); ?></div>
    <dov class="col-md-8 col-md-offset-2 col-xs-12">
        <div class="box box-primary">
            <?php
            /**
             * Open Form
             *
             * @var string
             **/
            echo form_open(current_url(), array('class' => 'form-horizontal'));
            ?>
            <div class="box-body" style="margin-top: 10px;">
                <div class="form-group">
                    <label for="name" class="control-label col-md-3 col-xs-12">Nama Kegiatan : <strong
                                class="text-red">*</strong></label>
                    <div class="col-md-8">
                        <input type="text" name="name" class="form-control"
                               value="<?php echo set_value('name'); ?>">
                        <p class="help-block"><?php echo form_error('name', '<small class="text-red">', '</small>'); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="date" class="control-label col-md-3 col-xs-12">Waktu Kegiatan : <strong
                                class="text-blue">*</strong></label>
                    <div class="col-md-8">
                        <input type="text" name="date" class="form-control"
                               value="<?php echo set_value('date'); ?>">
                        <p class="help-block"><?php echo form_error('date', '<small class="text-red">', '</small>'); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="students_limit" class="control-label col-md-3 col-xs-12">Tahun Akademik : <strong
                                class="text-blue">*</strong></label>
                    <div class="col-md-8">
                        <select name="thn_akademik" class="form-control">
                            <option value="">-- PILIH --</option>
                            <?php
                            /**
                             * Loop Tahun Ajaran
                             *
                             * @var Integer
                             **/
                            $thn2 = 2011;
                            for($thn1 = 2010; $thn1 <= (date('Y')); $thn1++) :
                                ?>
                                <option value="<?php echo $thn1.'/'.$thn2; ?>" <?php if(($thn1.'/'.$thn2)==set_value('thn_akademik')) echo "selected"; ?>><?php echo $thn1.'/'.$thn2; ?></option>
                                <?php
                                $thn2++;
                                // End Loop thn Ajaran
                            endfor;
                            ?>
                        </select>
                        <p class="help-block"><?php echo form_error('thn_akademik', '<small class="text-red">', '</small>'); ?></p>
                    </div>
                </div>
            </div>
            <div class="box-footer with-border">
                <div class="col-md-4 col-xs-5">
                    <a href="<?php echo site_url('akademik/kalender') ?>" class="btn btn-app pull-right">
                        <i class="ion ion-reply"></i> Kembali
                    </a>
                </div>
                <div class="col-md-6 col-xs-6">
                    <button type="submit" class="btn btn-app pull-right">
                        <i class="fa fa-save"></i> Simpan
                    </button>
                </div>
            </div>
            <div class="box-footer with-border">
                <small><strong class="text-red">*</strong> Field wajib diisi!</small>
                <br>
                <small><strong class="text-blue">*</strong> Field Optional</small>
            </div>
            <?php
            // End Form
            echo form_close();
            ?>
        </div>
    </dov>
</div>