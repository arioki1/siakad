<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dosen Crud Model
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Mkalender extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

	}	

	public function get_all($limit = 20, $offset = 0, $type = 'result')
	{
		if($this->input->get('query') != '')
			$this->db->like('name', $this->input->get('query'));
        if($this->input->get('thn_akademik') != '')
            $this->db->where('thn_akademik', $this->input->get('thn_akademik'));
		$this->db->order_by('id', 'desc');

		if($type == 'result')
		{
			return $this->db->get('kalender', $limit, $offset)->result();
		} else {
			return $this->db->get('kalender')->num_rows();
		}
	}

	public function get($param = '')
	{
		return $this->db->get_where('kalender', array('id' => $param))->row();
	}
	
	public function create()
	{
		$kalender = array(
			'name' => $this->input->post('name'),
			'date' => $this->input->post('date'),
			'thn_akademik' => $this->input->post('thn_akademik')
		);

		$this->db->insert('kalender', $kalender);

		if($this->db->affected_rows())
		{
			$this->template->alert(
				' Data Kalender Akademik ditambahkan.',
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menyimpan data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function update($param = 0)
	{
		$kalender= array(
			'name' => $this->input->post('name'),
			'date' => $this->input->post('date'),
			'thn_akademik' => $this->input->post('thn_akademik'),
		);

		$this->db->update('kalender', $kalender, array('id' => $param));

		if($this->db->affected_rows())
		{
			$this->template->alert(
				' Perubahan disimpan.', 
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menyimpan data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function delete($param = 0)
	{
		$this->db->delete('kalender', array('id' => $param));

		if($this->db->affected_rows())
		{
			$this->template->alert(
				' Ruangan terhapus.',
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menghapus data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function multiple_delete()
	{
		if(is_array($this->input->post('kalender')))
		{
			foreach ($this->input->post('kalender') as $key => $value)
			{
				$this->db->delete('kalender', array('id' => $value));
			}

			if($this->db->affected_rows())
			{
				$this->template->alert(
					' Ruangan terpilih dihapus.',
					array('type' => 'success','icon' => 'check')
				);
			} else {
				$this->template->alert(
					' Gagal menghapus data.', 
					array('type' => 'warning','icon' => 'times')
				);
			}
		}
	}

	/**
	 * Cek Validasi Kode
	 *
	 * @return Bolean
	 **/
	public function check_code()
	{
		$this->db->where('id', $this->input->post('id'));

		return $this->db->get('kalender')->num_rows();
	}
}

/* End of file Mlecturer.php */
/* Location: ./application/modules/akademik/models/Mlecturer.php */