<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dosen Crud Model
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Mconcentration extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

	}	

	public function get_all($limit = 20, $offset = 0, $type = 'result')
	{
		if($this->input->get('query') != '')
			$this->db->like('concentration_name', $this->input->get('query'))
                     ->or_like('concentration_kaprodi', $this->input->get('query'));

		$this->db->order_by('concentration_id', 'desc');

		if($type == 'result')
		{
			return $this->db->get('concentration', $limit, $offset)->result();
		} else {
			return $this->db->get('concentration')->num_rows();
		}
	}

	public function get($param = '')
	{
		return $this->db->get_where('concentration', array('concentration_id' => $param))->row();
	}
	
	public function create()
	{
		$concentration = array(
			'concentration_id' => $this->input->post('concentration_id'),
			'concentration_name' => $this->input->post('concentration_name'),
			'concentration_kaprodi' => $this->input->post('concentration_kaprodi')
		);

		$this->db->insert('concentration', $concentration);

		if($this->db->affected_rows())
		{
			$this->template->alert(
                ' Data Konsentrasi ditambahkan.',
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menyimpan data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function update($param = 0)
	{
        $concentration = array(
            'concentration_id' => $this->input->post('concentration_id'),
            'concentration_name' => $this->input->post('concentration_name'),
            'concentration_kaprodi' => $this->input->post('concentration_kaprodi')
        );

		$this->db->update('concentration', $concentration, array('concentration_id' => $param));

		if($this->db->affected_rows())
		{
			$this->template->alert(
				' Perubahan disimpan.', 
				array('type' => 'success','icon' => 'check')
			);

		} else {
			$this->template->alert(
				' Gagal menyimpan data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function delete($param = 0)
	{
		$this->db->delete('concentration', array('concentration_id' => $param));

		if($this->db->affected_rows())
		{
			$this->template->alert(
                ' Konsentrasi terhapus.',
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menghapus data.', 
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function multiple_delete()
	{
		if(is_array($this->input->post('concentration')))
		{
			foreach ($this->input->post('concentration') as $key => $value)
			{
				$this->db->delete('concentration', array('concentration_id' => $value));
			}

			if($this->db->affected_rows())
			{
				$this->template->alert(
                    ' Konsentrasi terpilih dihapus.',
					array('type' => 'success','icon' => 'check')
				);
			} else {
				$this->template->alert(
					' Gagal menghapus data.', 
					array('type' => 'warning','icon' => 'times')
				);
			}
		}
	}

	/**
     * Cek Validasi Kode Konsentrasi
	 *
	 * @return Bolean
	 **/
	public function check_code()
	{
		$this->db->where('concentration_id', $this->input->post('concentration_id'));

		return $this->db->get('concentration')->num_rows();
	}

    public function get_concentration()
    {
        return $this->db->get("concentration")->result_array();
    }
}

/* End of file Mlecturer.php */
/* Location: ./application/modules/akademik/models/Mlecturer.php */