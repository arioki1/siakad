<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dosen Crud Model
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Mdosenpa extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

	}

	public function get_all($limit = 20, $offset = 0, $type = 'result')
	{
        $this->db->select('dosen_pa.lecturer_id as "id_pa",
                        lecturer.lecturer_code as "kode_dosen",
                        lecturer.name as "nama_dosen",
                        COUNT(students.name) as "jumlah_mahasiswa"');

        $this->db->join('lecturer', 'dosen_pa.lecturer_id = lecturer.lecturer_id', 'inner');
        $this->db->join('students', 'dosen_pa.lecturer_id = students.dosen_pa', 'left');
        $this->db->where('lecturer.status',"ds_tetap");
        $this->db->group_by('dosen_pa.ID');

		if($this->input->get('query') != '')
			$this->db->like('lecturer.name', $this->input->get('query'))
		            ->or_like('lecturer.lecturer_code', $this->input->get('query'));

        $this->db->group_by('lecturer.lecturer_code','DESC');

		if($type == 'result')
		{
			return $this->db->get('dosen_pa', $limit, $offset)->result();
		} else {
			return $this->db->get('dosen_pa')->num_rows();
		}
	}

	public function delete($param = 0)
	{
		$this->db->delete('dosen_pa', array('lecturer_id' => $param));
		if($this->db->affected_rows())
		{
		    $this->delete_siswa_pa($param);
			$this->template->alert(
				' Dosen PA telah dihapus.',
				array('type' => 'success','icon' => 'check')
			);
		} else {
			$this->template->alert(
				' Gagal menghapus data.',
				array('type' => 'warning','icon' => 'times')
			);
		}
	}

	public function multiple_add_dosen_pa()
	{

		if(is_array($this->input->post('lecturer')))
		{
			foreach ($this->input->post('lecturer') as $key => $value)
			{
                $lecturer = array(
                    'lecturer_id' => $value
                );

                $this->db->insert('dosen_pa', $lecturer);
			}

			if($this->db->affected_rows())
			{
				$this->template->alert(
					' Ruangan terpilih dihapus.',
					array('type' => 'success','icon' => 'check')
				);
			} else {
				$this->template->alert(
					' Gagal menghapus data.',
					array('type' => 'warning','icon' => 'times')
				);
			}
		}
	}

	public function delete_siswa_pa($id){
        $this->db->set('dosen_pa', 'NULL', FALSE);
        $this->db->where('dosen_pa', $id);
        $this->db->update('students');
    }

    public function get_all_dosen($limit = 20, $offset = 0, $type = 'result')
    {


        if($this->input->get('status') != '')
        {
            $query = "SELECT * FROM lecturer 
                    WHERE
                    lecturer.lecturer_id NOT IN ( SELECT dosen_pa.lecturer_id FROM dosen_pa )
                    AND lecturer.`status` = 'ds_tetap'
                    ORDER BY lecturer_id DESC";
        }else{
            $query = "SELECT * FROM lecturer 
                    WHERE
                    lecturer.lecturer_id NOT IN ( SELECT dosen_pa.lecturer_id FROM dosen_pa )
                    AND lecturer.`status` = 'ds_tetap'
                    ORDER BY lecturer_id DESC
                    LIMIT ".$limit;
        }


        if($type == 'result')
        {
            return $this->db->query($query)->result();
        } else {
            return $this->db->get('lecturer')->num_rows();
        }
    }

}

/* End of file Mlecturer.php */
/* Location: ./application/modules/akademik/models/Mlecturer.php */