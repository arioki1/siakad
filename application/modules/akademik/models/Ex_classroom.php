<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ex_classroom extends CI_Model
{
	protected $ci;

	public function __construct()
	{
		parent::__construct();
		$this->ci = $ci =& get_instance();
		// load library
		$this->load->library(array('Excel/PHPExcel', 'upload'));
		$this->load->model('moption', 'option');
		ini_set('max_execution_time', 3000); 
	}
	
	/**
	 * Set Import Data Kelas From Excel
	 *
	 * @package  PHPExcel
	 * @see Documentation (https://github.com/PHPOffice/PHPExcel/wiki)
	 * @return string
	 **/
	public function set()
	{
		$config['upload_path'] = 'assets/excel';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']  = '5120';
		
		$this->upload->initialize($config);
		
		if ( ! $this->upload->do_upload('file_excel')) 
		{
			$output = array('status' => 'ERROR', 'message' => $this->upload->display_errors('<span>','</span>'));
		} else {

			$file_excel = "./assets/excel/{$this->upload->file_name}";

			// Identifikasi File Excel Reader
			try {

				$excelReader = new PHPExcel_Reader_Excel2007();

            	$loadExcel = $excelReader->load($file_excel);	

            	$sheet = $loadExcel->getActiveSheet()->toArray(null, true, true ,true);
		        // Loops Excel data reader

		        foreach ($sheet as $key => $value) 
		        {
		        	// Mulai Dari Baris ketiga
		        	if($key > 3)
		        	{
                        $classroom = array(
                            'class_name' => $value['B'],
                            'students_limit' => $value['C'],
                            'description' => $value['D']
                        );

                        $this->db->insert('classroom', $classroom);

		        	// End Baris ketiga
		        	}
		        // End Loop
		        }

		        unlink("./assets/excel/{$this->upload->file_name}");

				$output = array(
					'status' => 'OK', 
					'message' => ' Data Kelas berhasil diimport.'
				);
			} catch (Exception $e) {
				$output = array(
					'status' => 'ERROR', 
					'message' => 'Error loading file "'.pathinfo($file_excel,PATHINFO_BASENAME).'": '.$e->getMessage()
				);
			}
		}

		echo json_encode($output);
	}

	/**
	 * Cek Validasi Kelas
	 *
	 * @param String = npm 
	 * @return Boolean
	 **/
	public function getNpm($param = 0)
	{
		$query = $this->db->query("SELECT npm FROM students WHERE npm = ?", $param);
		if($query->num_rows())
		{
			return TRUE;
		} else {
			return FALSE;
		}
	}

    /**
     * Export Data Kelas ke Excel
     *
     * @param Array (Kelas Result)
     * @return Attachment excel
     **/
    public function get()
    {
        $objPHPExcel = new PHPExcel();

        $worksheet = $objPHPExcel->createSheet(0);

        for ($cell='A'; $cell <= 'G'; $cell++)
        {
            $worksheet->getStyle($cell.'1')->getFont()->setBold(true);
        }

        $worksheet->getStyle('A1:G1')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'F2F2F2')
                )
            )
        );

        // Header dokumen
        $worksheet->setCellValue('A1', 'NO.')
            ->setCellValue('B1', 'Nama Kelas')
            ->setCellValue('C1', 'Kapasitas')
            ->setCellValue('D1', 'Kapasitas');

        $row_cell = 2;
        foreach($this->db->get('classroom')->result() as $key => $value)
        {
            $worksheet->setCellValue('A'.$row_cell, ++$key)
                ->setCellValue('B'.$row_cell, $value->class_name)
                ->setCellValue('C'.$row_cell, $value->students_limit)
                ->setCellValue('D'.$row_cell, $value->description);
            $row_cell++;
        }

        // Sheet Title
        $worksheet->setTitle("DATA KELAS");

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');\
        header('Content-Disposition: attachment; filename="DATA-KELAS.xlsx"');
        $objWriter->save("php://output");
    }
}
