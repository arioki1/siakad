<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dosen Crud Controller
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Dosenpa extends Akademik
{
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('akademik'));

		$this->breadcrumbs->unshift(1, 'Master', "akademik/classroom");

		$this->load->model('mdosenpa', 'dosenpa');

        //$this->load->model('ex_mdosenpa');

		$this->load->js(base_url("assets/app/akademik/dosenpa.js"));
	}

	public function index()
	{
		$this->page_title->push('Master', 'Data Dosen PA');

		$this->breadcrumbs->unshift(2, 'Data Dosen PA', "akademik/dosenpa");

		$field = array(
			$this->input->get('per_page'),
			$this->input->get('query')
		);

		// set pagination
		$config = $this->template->pagination_list();

		$config['base_url'] = site_url("akademik/dosenpa?per_page={$field[0]}&query={$field[0]}");

		$config['per_page'] = (!$this->input->get('per_page')) ? 20 : $this->input->get('per_page');
		$config['total_rows'] = $this->dosenpa->get_all(null, null, 'num');

		$this->pagination->initialize($config);

		$this->data = array(
			'title' => "Data Dosen PA",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
			'daftar_dosen' => $this->dosenpa->get_all($config['per_page'], $this->input->get('page')),
			'jumlah_dosen' => $config['total_rows']
		);

		$this->template->view('master/data-dosen-pa', $this->data);
	}

	public function add()
	{

        $this->page_title->push('Master', 'Data Dosen PA');

        $this->breadcrumbs->unshift(2, 'Data Dosen', "akademik/lecturer");

        $field = array(
            $this->input->get('per_page'),
            $this->input->get('status'),
            $this->input->get('query')
        );

        // set pagination
        $config = $this->template->pagination_list();

        $config['base_url'] = site_url("akademik/dosenpa/add?per_page={$field[0]}&status={$field[1]}&query={$field[0]}");

        $config['per_page'] = (!$this->input->get('per_page')) ? 20 : $this->input->get('per_page');
        $config['total_rows'] = $this->dosenpa->get_all_dosen(null, null, 'num');

        $this->pagination->initialize($config);

        $this->data = array(
            'title' => "Data Dosen",
            'breadcrumb' => $this->breadcrumbs->show(),
            'page_title' => $this->page_title->show(),
            'js' => $this->load->get_js_files(),
            'daftar_dosen' => $this->dosenpa->get_all_dosen($config['per_page'], $this->input->get('page')),
            'jumlah_dosen' => $config['total_rows']
        );

		$this->template->view('master/add-data-dosen-pa', $this->data);
	}


	public function delete($param = 0)
	{
		$this->dosenpa->delete($param);

		redirect("akademik/dosenpa");
	}

	/**
	 * Multiple Action
	 *
	 * @return string
	 **/
	public function bulk_action()
	{
		switch ($this->input->post('action')) 
		{
			case 'delete':
				$this->dosenpa->multiple_delete();
				break;
			
			default:
				$this->template->alert(
					' Tidak ada aksi apapun.', 
					array('type' => 'warning','icon' => 'times')
				);
				break;
		}

		redirect('akademik/dosenpa');


	}/**
	 * Multiple Action
	 *
	 * @return string
	 **/
	public function bulk_action_add_dosen_pa()
	{
		switch ($this->input->post('action'))
		{
			case 'add':
				$this->dosenpa->multiple_add_dosen_pa();
				break;

			default:
				$this->template->alert(
					' Tidak ada aksi apapun.',
					array('type' => 'warning','icon' => 'times')
				);
				break;
		}

		redirect('akademik/dosenpa');
	}

	/**
	 * Cek Validasi Kode Kelas
	 *
	 * @return Bolean
	 **/
	public function validate_dscode()
	{
		if($this->classroom->check_code() == TRUE)
		{
			$this->form_validation->set_message('validate_dscode', 'Maaf! Nama Kelas telah digunakan.');
			return false;
		} else {
			return true;
		}
	}

    /**
     * Get halaman Import Data
     *
     * @return string
     **/
    public function import()
    {
        $this->page_title->push('Ruang Kelas', 'Import Data Kelas');

        $this->breadcrumbs->unshift(2, 'Import Data', "akademik/classroom/add");

        $this->data = array(
            'title' => "Import data kelas",
            'breadcrumb' => $this->breadcrumbs->show(),
            'page_title' => $this->page_title->show(),
            'js' => $this->load->get_js_files(),
        );

        $this->template->view('master/import-ruang-kelas', $this->data);
    }

    public function set_import()
    {
        $this->ex_classroom->set();
    }

    /**
     * Get Download Export
     *
     * @return Attachment
     **/
    public function get_export()
    {
        $this->ex_classroom->get();
    }
}

/* End of file Lecturer.php */
/* Location: ./application/modules/akademik/controllers/Lecturer.php */