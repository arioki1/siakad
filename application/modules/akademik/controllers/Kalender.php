<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dosen Crud Controller
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Kalender extends Akademik
{
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('akademik'));

		$this->breadcrumbs->unshift(1, 'Kalender', "akademik/kalender");

		$this->load->model('mkalender', 'kalender');

		$this->load->js(base_url("assets/app/akademik/kalender.js"));
	}

	public function index()
	{
		$this->page_title->push('Kalender', 'Data Kalender Akadamik');

		$this->breadcrumbs->unshift(2, 'Data Kelender Akadmik', "akademik/kalender");

		$field = array(
			$this->input->get('per_page'),
			$this->input->get('query')
		);

		// set pagination
		$config = $this->template->pagination_list();

		$config['base_url'] = site_url("akademik/kalender?per_page={$field[0]}&query={$field[0]}");
		$config['per_page'] = (!$this->input->get('per_page')) ? 20 : $this->input->get('per_page');
		$config['total_rows'] = $this->kalender->get_all(null, null, 'num');

		$this->pagination->initialize($config);

		$this->data = array(
			'title' => "Data Kalender Akedemik",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
			'daftar_kalender' => $this->kalender->get_all($config['per_page'], $this->input->get('page')),
			'jumlah_kalender' => $config['total_rows']
		);

		$this->template->view('student/data-kalender', $this->data);
	}

	public function add()
	{
		$this->page_title->push('Kalender', 'Tambah Kalender Akademik');

		$this->breadcrumbs->unshift(2, 'Data Kelender Akadmik', "akademik/kalender");
		$this->form_validation->set_rules('name','Nama Kegiatan', 'trim|required');
		$this->form_validation->set_rules('date','Waktu Kegiatan', 'trim|required');
		$this->form_validation->set_rules('thn_akademik','Tahun Akademik', 'trim|required');

		if($this->form_validation->run() == TRUE)
		{
			$this->kalender->create();
			redirect('akademik/kalender/add');
		}

		$this->data = array(
			'title' => "Data Kalender Akadmik",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
		);

		$this->template->view('student/add-kalender', $this->data);
	}

	public function update($param = 0)
	{
		$this->page_title->push('Kalender', 'Update Data Kalender Akademik');

		$this->breadcrumbs->unshift(2, 'Data Kalender Akademik', "akademik/kalender");

        $this->form_validation->set_rules('name','Nama Kegiatan', 'trim|required');
        $this->form_validation->set_rules('date','Waktu Kegiatan', 'trim|required');
        $this->form_validation->set_rules('thn_akademik','Tahun Akademik', 'trim|required');

		if($this->form_validation->run() == TRUE)
		{
			$this->kalender->update($param);

			redirect("akademik/kalender/update/{$param}");
		}

		$this->data = array(
			'title' => "Update Data Kalender Akadmik",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
			'get' => $this->kalender->get($param)
		);

		$this->template->view('student/update-kalender', $this->data);
	}

	public function delete($param = 0)
	{
		$this->kalender->delete($param);

		redirect("akademik/kalender");
	}

	/**
	 * Multiple Action
	 *
	 * @return string
	 **/
	public function bulk_action()
	{
		switch ($this->input->post('action')) 
		{
			case 'delete':
				$this->kalender->multiple_delete();
				break;
			
			default:
				$this->template->alert(
					' Tidak ada aksi apapun.', 
					array('type' => 'warning','icon' => 'times')
				);
				break;
		}

		redirect('akademik/kalender');
	}

	/**
	 * Cek Validasi
	 *
	 * @return Bolean
	 **/
	public function validate_dscode()
	{
		if($this->kalender->check_code() == TRUE)
		{
			$this->form_validation->set_message('validate_dscode', 'Maaf! kode telah digunakan.');
			return false;
		} else {
			return true;
		}
	}
}

/* End of file Lecturer.php */
/* Location: ./application/modules/akademik/controllers/Lecturer.php */