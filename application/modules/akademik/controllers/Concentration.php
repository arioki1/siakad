<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dosen Crud Controller
 *
 * @package Bag. Akademik
 * @category Akademik or Super Admin
 * @see https://github.com/nitinegoro/siakad-terpadu
 * @author Vicky Nitinegoro
 **/

class Concentration extends Akademik
{
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('akademik'));

		$this->breadcrumbs->unshift(1, 'Master', "akademik/concentration");

		$this->load->model('mconcentration', 'concentration');

        $this->load->model('ex_concentration');

		$this->load->js(base_url("assets/app/akademik/concentration.js"));
	}

	public function index()
	{
        $this->page_title->push('Master', 'Data Konsentrasi');

        $this->breadcrumbs->unshift(2, 'Data Konsentrasi', "akademik/concentration");

		$field = array(
			$this->input->get('per_page'),
			$this->input->get('query')
		);

		// set pagination
		$config = $this->template->pagination_list();

		$config['base_url'] = site_url("akademik/concentration?per_page={$field[0]}&query={$field[0]}");

		$config['per_page'] = (!$this->input->get('per_page')) ? 20 : $this->input->get('per_page');
		$config['total_rows'] = $this->concentration->get_all(null, null, 'num');

		$this->pagination->initialize($config);

		$this->data = array(
			'title' => "Data Ruang Kelas",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
            'daftar_konsentrasi' => $this->concentration->get_all($config['per_page'], $this->input->get('page')),
            'jumlah_konsentrasi' => $config['total_rows']
		);

		$this->template->view('master/data-concentration', $this->data);
	}

	public function add()
	{
        $this->page_title->push('Master', 'Tambah konsentrasi Baru');

		$this->breadcrumbs->unshift(2, 'Data Ruang Kelas', "akademik/concentration");

        $this->form_validation->set_rules('concentration_id', 'ID Konsentrasi', 'trim');
        $this->form_validation->set_rules('concentration_name', 'Nama Konsentrasi', 'trim');
		$this->form_validation->set_rules('concentration_kaprodi', 'Nama Kaprodi', 'trim');

		if($this->form_validation->run() == TRUE)
		{
			$this->concentration->create();

			redirect('akademik/concentration/add');
		}

		$this->data = array(
            'title' => "Data Konsentrasi",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
		);

		$this->template->view('master/add-concentration', $this->data);
	}

	public function update($param = 0)
	{
        $this->page_title->push('Master', 'Update Data Konsentrasi');

        $this->breadcrumbs->unshift(2, 'Data Konsentrasi', "akademik/concentration");

        $this->form_validation->set_rules('concentration_id', 'ID Konsentrasi', 'trim');
        $this->form_validation->set_rules('concentration_name', 'Nama Konsentrasi', 'trim');
        $this->form_validation->set_rules('concentration_kaprodi', 'Nama Kaprodi', 'trim');

        if($this->form_validation->run() == TRUE)
		{
			$this->concentration->update($param);

			redirect("akademik/concentration/update/{$this->input->post('concentration_id')}");
		}

		$this->data = array(
            'title' => "Update Data Konsentrasi",
			'breadcrumb' => $this->breadcrumbs->show(),
			'page_title' => $this->page_title->show(),
			'js' => $this->load->get_js_files(),
			'get' => $this->concentration->get($param)
		);

		$this->template->view('master/update-concentration', $this->data);
	}

	public function delete($param = 0)
	{
		$this->concentration->delete($param);

		redirect("akademik/concentration");
	}

	/**
	 * Multiple Action
	 *
	 * @return string
	 **/
	public function bulk_action()
	{
		switch ($this->input->post('action')) 
		{
			case 'delete':
				$this->concentration->multiple_delete();
				break;
			
			default:
				$this->template->alert(
					' Tidak ada aksi apapun.', 
					array('type' => 'warning','icon' => 'times')
				);
				break;
		}

		redirect('akademik/concentration');
	}

	/**
	 * Cek Validasi Kode Kelas
	 *
	 * @return Bolean
	 **/
	public function validate_dscode()
	{
		if($this->concentration->check_code() == TRUE)
		{
			$this->form_validation->set_message('validate_dscode', 'Maaf! Nama Kelas telah digunakan.');
			return false;
		} else {
			return true;
		}
	}

    /**
     * Get halaman Import Data
     *
     * @return string
     **/
    public function import()
    {
        $this->page_title->push('Data Fakultas', 'Import Data Konsentrasi');

        $this->breadcrumbs->unshift(2, 'Import Data', "akademik/concentration/add");

        $this->data = array(
            'title' => "Import Data Konsentrasi",
            'breadcrumb' => $this->breadcrumbs->show(),
            'page_title' => $this->page_title->show(),
            'js' => $this->load->get_js_files(),
        );

        $this->template->view('master/import-concentration', $this->data);
    }

    public function set_import()
    {
        $this->ex_concentration->set();
    }

    /**
     * Get Download Export
     *
     * @return Attachment
     **/
    public function get_export()
    {
        $this->ex_concentration->get();
    }
}
