<?php
/**
 * Created by PhpStorm.
 * User: Ari Oki
 * Date: 30/03/2019
 * Time: 19.17
 */

class Mmain extends CI_Model
{
    public $thn_ajaran;
    public $semester;
    public function __construct()
    {
    	parent::__construct();
        $this->thn_ajaran = (!$this->input->get('thn_ajaran')) ? $this->option->get('default_thn_ajaran') : $this->input->get('thn_ajaran');
        $this->semester = (!$this->input->get('semester')) ? $this->option->get('default_semester') : $this->input->get('semester');
    }
    function getCalendar(){
        $this->db->where('thn_akademik', $this->thn_ajaran);
        return $this->db->get("kalender")->result();
    }
}