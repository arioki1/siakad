<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="col-lg-12 col-xs-12">
            <div class="callout callout-info">
                <h4>Informasi</h4>
                <p>- Baca penggunaan sistem pada menu panduan.</p>
                <p>- Silahkan Ganti password anda dari password yang telah ditentukan.</p>
                <p>- Lengkapi data diri anda pada menu Akun (
                    <small><i>pojok kanan atas</i></small>
                    ).
                </p>
                <p>- Hubungi Bag. SIM melalui Chat Online.</p>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 hidden-xs">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        <?php echo str_replace('.', ',', substr($total_ipk, 0, 5)); ?>
                    </h3>
                    <p>Total IPK Sementara</p>
                </div>
                <div class="icon">
                    <i class="ion ion-trophy"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 hidden-xs">
            <!-- small box -->
            <div class="small-box bg-teal">
                <div class="inner">
                    <h3>
                        <?php echo $total_sks; ?>
                    </h3>
                    <p>SKS sudah ditempuh</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-bookmarks"></i>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-6 col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-calendar"></i> Kalendar Akademik Tahun <?php print_r($thn_ajaran) ?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th width="30">No.</th>
                        <th>Kegiatan</th>
                        <th>Tanggal</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 0;
                    foreach ($kalender_akademik as $row){
                        $no++;
                    echo '<tr>
                            <td>'.$no.'</td>
                            <td>'.$row->name.'</td>
                            <td><i>'.$row->date.'</i></td>
                           </tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>


</div>


