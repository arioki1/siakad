jQuery(document).ready(function($)
{
    jQuery('time.timeago').timeago();
    
    $(".btn-print").printPage();
});

$(function () {

    $('table').tableCheckbox({
        selectedRowClass: 'info',
        checkboxSelector: 'td:first-of-type input[type="checkbox"],th:first-of-type input[type="checkbox"]',
        isChecked: function($checkbox) {
            return $checkbox.is(':checked');
        }
    });
    
	 $('[data-toggle="tooltip"]').tooltip();

    // Uploads IMPORT data Buku
    $('#form-import-mata-kuliah').formValidation({
        excluded: [':disabled'],
        fields: {
            file_excel: {
                validators: {
                    notEmpty: {  message: 'Harap isi File.' },
                    file: { extension: 'xlsx', maxSize: 97152 }
                }
            }
        }
    })
        .on('success.form.fv', function(e) {

            e.preventDefault();

            var notify = $.notify('<strong>Mengunggah</strong> jangan tinggalkan halaman ini...', {
                type: 'success',
                allow_dismiss: false,
                showProgressbar: true
            });

            setTimeout(function() {
                notify.update({'message': '<strong>Membaca</strong> file excel ....', 'progress': 25});
            }, 40000);

            setTimeout(function() {
                notify.update({'type':'info','message': '<strong>Mengimport</strong> Data Mata Kuliah...', 'progress': 45});
            }, 60000);

            var $form     = $(e.target);

            $.ajaxFileUpload({
                url : base_url + '/course/set_import',
                secureuri : false,
                fileElementId :'file-excel',
                dataType : 'json',
                success : function (res)
                {
                    if(res.status === 'OK')
                    {
                        $.notify({
                            icon: 'fa fa-check',
                            message: res.message
                        },{
                            type: 'success',
                            allow_dismiss: false,
                            delay:2000,
                            placement: {
                                from: "top",
                                align: "center"
                            },
                        });
                    } else {
                        $.notify({
                            icon: 'fa fa-warning',
                            message: res.message
                        },{
                            type: 'warning',
                            allow_dismiss: false,
                            delay:2000,
                            placement: {
                                from: "top",
                                align: "center"
                            },
                        });
                    }

                    $form.formValidation('disableSubmitButtons', false).formValidation('resetForm', true);
                },
                error: function(res)
                {

                }
            });
            return false;
        });
});
